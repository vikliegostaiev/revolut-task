const CONVERT_TO_COINS_MULTIPLIER = 100

export function convertAmountToTwoDigits(balance) {
  return balance / CONVERT_TO_COINS_MULTIPLIER
}

export function convertToCoins(amount) {
  return Math.round(amount * CONVERT_TO_COINS_MULTIPLIER)
}
