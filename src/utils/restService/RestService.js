import axios from 'axios'

export async function getCurrency(base, result) {
  try {
    const response = await axios.get('https://api.exchangeratesapi.io/latest', {
      params: {
        base,
        symbols: result,
      },
    })

    if (response) {
      return response.data
    }
  } catch (err) {
    console.error(err)
  }
}
