import { getTransferActionType } from '../action/actionTypes'

function walletReducerFactory(currency, currencySymbol, defaultBalance) {
  const initialState = {
    currency,
    currencySymbol,
    balance: defaultBalance,
  }

  return (state = initialState, action) => {
    switch (action.type) {
      case getTransferActionType(currency):
        return {
          ...state,
          balance: state.balance + action.amount,
        }
      default:
        return state
    }
  }
}

export { walletReducerFactory }
