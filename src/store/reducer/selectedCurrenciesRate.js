import { SET_EXCHANGE_RATE } from '../action/actionTypes'

const initialState = {
  rate: null,
}

const selectedCurrenciesRate = (state = initialState, action) => {
  switch (action.type) {
    case SET_EXCHANGE_RATE:
      return {
        rate: action.newRate,
      }
    default:
      return state
  }
}

export { selectedCurrenciesRate }
