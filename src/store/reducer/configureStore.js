import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import { walletReducerFactory } from './walletReducerFactory'
import { createSelectedWalletsReducer } from './selectedWalletsReducerFactory'
import { selectedCurrenciesRate } from './selectedCurrenciesRate'

const getReducersFromWalletArray = wallets => {
  const reducers = {}

  wallets.forEach(wallet => {
    reducers[wallet.currency.toLowerCase()] = walletReducerFactory(
      wallet.currency,
      wallet.currencySymbol,
      wallet.balance,
    )
  })

  return reducers
}

const getSelectedWalletsReducer = wallets => {
  if (wallets.length > 1) {
    return createSelectedWalletsReducer(wallets[0], wallets[1])
  }

  return createSelectedWalletsReducer(wallets[0], wallets[0])
}

const combineRootReducer = walletsData => {
  let reducers = {}

  if (walletsData && walletsData.wallets && walletsData.wallets.length) {
    reducers = getReducersFromWalletArray(walletsData.wallets)
    reducers.selectedWallets = getSelectedWalletsReducer(walletsData.wallets)
  }

  return combineReducers({ ...reducers, selectedCurrenciesRate })
}

const configureStore = walletsData => {
  return createStore(
    combineRootReducer(walletsData),
    applyMiddleware(thunk)
  )
}

export { configureStore }
