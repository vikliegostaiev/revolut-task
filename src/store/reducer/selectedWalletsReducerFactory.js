import {
  SET_NEW_ORIGIN_WALLET,
  SET_NEW_DESTINATION_WALLET,
  SET_NEW_SELECTED_WALLETS,
  CHANGE_ORIGIN_INPUT_VALUE,
  CHANGE_DESTINATION_INPUT_VALUE,
  SET_ORIGIN_INPUT_FIELD_FOCUS,
  SET_DESTINATION_INPUT_FIELD_FOCUS,
  getTransferActionType,
} from '../action/actionTypes'

function createSelectedWalletsReducer(originWallet, destinationWallet) {
  const initialState = {
    originWallet: {
      ...originWallet,
      amount: '',
      lastFocusedInputField: false,
    },
    destinationWallet: {
      ...destinationWallet,
      amount: '',
      lastFocusedInputField: false,
    },
  }

  return (state = initialState, action) => {
    switch (action.type) {
      case SET_NEW_ORIGIN_WALLET:
        return {
          ...state,
          originWallet: { ...action.wallet, amount: state.originWallet.amount },
        }
      case SET_NEW_DESTINATION_WALLET:
        return {
          ...state,
          destinationWallet: {
            ...action.wallet,
            amount: state.destinationWallet.amount,
          },
        }
      case SET_NEW_SELECTED_WALLETS:
        return {
          originWallet: {
            ...action.originWallet,
            amount: state.destinationWallet.lastFocusedInputField
              ? state.destinationWallet.amount
              : state.originWallet.amount,
          },
          destinationWallet: {
            ...action.destinationWallet,
            amount: state.originWallet.lastFocusedInputField
              ? state.originWallet.amount
              : state.destinationWallet.amount,
          },
        }
      case CHANGE_ORIGIN_INPUT_VALUE:
        return {
          ...state,
          originWallet: { ...state.originWallet, amount: action.newInputValue },
        }
      case CHANGE_DESTINATION_INPUT_VALUE:
        return {
          ...state,
          destinationWallet: {
            ...state.destinationWallet,
            amount: action.newInputValue,
          },
        }
      case SET_ORIGIN_INPUT_FIELD_FOCUS:
        return {
          originWallet: { ...state.originWallet, lastFocusedInputField: true },
          destinationWallet: {
            ...state.destinationWallet,
            lastFocusedInputField: false,
          },
        }
      case SET_DESTINATION_INPUT_FIELD_FOCUS:
        return {
          originWallet: { ...state.originWallet, lastFocusedInputField: false },
          destinationWallet: {
            ...state.destinationWallet,
            lastFocusedInputField: true,
          },
        }
      case getTransferActionType(state.originWallet.currency):
        return {
          ...state,
          originWallet: {
            ...state.originWallet,
            balance: state.originWallet.balance + action.amount,
          },
        }
      case getTransferActionType(state.destinationWallet.currency):
        return {
          ...state,
          destinationWallet: {
            ...state.destinationWallet,
            balance: state.destinationWallet.balance + action.amount,
          },
        }
      default:
        return state
    }
  }
}

export { createSelectedWalletsReducer }
