import { getTransferActionType } from './actionTypes'

const transferNewAmount = (amount, destinationWalletCurrency) => {
  return {
    amount,
    type: getTransferActionType(destinationWalletCurrency),
  }
}

export const makeTransfer = () => (dispatch, getState) => {
  const originWallet = getState().selectedWallets.originWallet
  const destinationWallet = getState().selectedWallets.destinationWallet
  dispatch(transferNewAmount(originWallet.amount * -1, originWallet.currency))
  dispatch(
    transferNewAmount(destinationWallet.amount, destinationWallet.currency),
  )
  alert('Transfer successfully')
}
