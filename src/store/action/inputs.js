import {
  CHANGE_DESTINATION_INPUT_VALUE,
  CHANGE_ORIGIN_INPUT_VALUE,
  SET_DESTINATION_INPUT_FIELD_FOCUS,
  SET_ORIGIN_INPUT_FIELD_FOCUS,
} from './actionTypes'

export const changeOriginInputValue = value => dispatch =>
  dispatch({
    newInputValue: value,
    type: CHANGE_ORIGIN_INPUT_VALUE,
  })

export const changeDestinationInputValue = value => dispatch =>
  dispatch({
    newInputValue: value,
    type: CHANGE_DESTINATION_INPUT_VALUE,
  })

export const setOriginInputFieldFocused = () => dispatch =>
  dispatch({
    type: SET_ORIGIN_INPUT_FIELD_FOCUS,
  })

export const setDestinationInputFieldFocused = () => dispatch =>
  dispatch({
    type: SET_DESTINATION_INPUT_FIELD_FOCUS,
  })

export const changeOriginInputValueBasedOnDestination = newDestinationInputValue => (
  dispatch,
  getState,
) => {
  const rate = getState().selectedCurrenciesRate.rate

  if (rate) {
    const newOriginInputValue = newDestinationInputValue
      ? Math.floor(newDestinationInputValue / rate)
      : newDestinationInputValue
    dispatch(changeOriginInputValue(newOriginInputValue))
  }
}

export const changeDestinationInputValueBasedOnOrigin = newOriginInputValue => (
  dispatch,
  getState,
) => {
  const rate = getState().selectedCurrenciesRate.rate

  if (rate) {
    const newDestinationInputValue = newOriginInputValue
      ? Math.floor(newOriginInputValue * rate)
      : newOriginInputValue
    dispatch(changeDestinationInputValue(newDestinationInputValue))
  }
}
