import { SET_EXCHANGE_RATE } from './actionTypes'
import {
  changeOriginInputValueBasedOnDestination,
  changeDestinationInputValueBasedOnOrigin,
} from './inputs'
import { getCurrency } from '../../utils/restService/RestService'

const setNewRate = newRate => {
  return {
    newRate,
    type: SET_EXCHANGE_RATE,
  }
}

export const fetchRate = () => async (dispatch, getState) => {
  const prevRate = getState().selectedCurrenciesRate.rate
  const { originWallet, destinationWallet } = getState().selectedWallets

  try {
    const response = await getCurrency(
      originWallet.currency,
      destinationWallet.currency,
    )
    const newRate = response.rates[destinationWallet.currency]
    dispatch(setNewRate(newRate))

    if (prevRate !== newRate) {
      if (destinationWallet.lastFocusedInputField && destinationWallet.amount) {
        dispatch(
          changeOriginInputValueBasedOnDestination(destinationWallet.amount),
        )
      } else if (originWallet.amount) {
        dispatch(changeDestinationInputValueBasedOnOrigin(originWallet.amount))
      }
    }
  } catch (err) {
    console.error(err)
  }
}
