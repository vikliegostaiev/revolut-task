import {
  SET_NEW_DESTINATION_WALLET,
  SET_NEW_ORIGIN_WALLET,
  SET_NEW_SELECTED_WALLETS,
} from './actionTypes'
import { fetchRate } from './rate'

const changeOriginWallet = wallet => {
  return {
    wallet,
    type: SET_NEW_ORIGIN_WALLET,
  }
}

const changeDestinationWallet = wallet => {
  return {
    wallet,
    type: SET_NEW_DESTINATION_WALLET,
  }
}

const changeBothWallets = (originWallet, destinationWallet) => {
  return {
    originWallet,
    destinationWallet,
    type: SET_NEW_SELECTED_WALLETS,
  }
}

export const setOriginWallet = newWallet => (dispatch, getState) => {
  const destinationWallet = getState().selectedWallets.destinationWallet

  if (newWallet.currency === destinationWallet.currency) {
    dispatch(
      changeBothWallets(
        {
          ...newWallet,
          lastFocusedInputField: destinationWallet.lastFocusedInputField,
        },
        getState().selectedWallets.originWallet,
      ),
    )
  } else {
    dispatch(changeOriginWallet(newWallet))
  }

  dispatch(fetchRate())
}

export const setDestinationWallet = newWallet => (dispatch, getState) => {
  const originWallet = getState().selectedWallets.originWallet

  if (newWallet.currency === originWallet.currency) {
    dispatch(
      changeBothWallets(getState().selectedWallets.destinationWallet, {
        ...newWallet,
        lastFocusedInputField: originWallet.lastFocusedInputField,
      }),
    )
  } else {
    dispatch(changeDestinationWallet(newWallet))
  }

  dispatch(fetchRate())
}

export const swapWallets = () => (dispatch, getState) => {
  const originWallet = getState().selectedWallets.originWallet
  const destinationWallet = getState().selectedWallets.destinationWallet

  dispatch(changeBothWallets(destinationWallet, originWallet))
  dispatch(fetchRate())
}
