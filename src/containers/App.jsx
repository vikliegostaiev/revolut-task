import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { GlobalStyle } from './GlobalStyles'
import ExchangeButton from '../components/exchangeButton/ExchangeButton'
import OriginWalletDisplay from '../components/walletDisplay/OriginWalletDisplay'
import DestinationWalletDisplay from '../components/walletDisplay/DestinationWalletDisplay'
import RateDisplay from '../components/rateDisplay/RateDisplay'
import SwapWallets from '../components/swapWallets/SwapWallets'
import { fetchRate } from '../store/action/rate'
import { connect } from 'react-redux'

const AppContainer = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

const Box = styled.div`
  position: relative;
  box-shadow: 0px 0px 12px 3px rgba(0, 0, 0, 0.42);
  height: 500px;
  width: 500px;
  display: flex;
  flex-direction: column;
`

const REFRESH_TIMEOUT = 10000

export class App extends React.Component {
  static propTypes = {
    fetchRate: PropTypes.func,
  }

  componentDidMount() {
    this.getCurrencyRate()
  }

  componentWillUnmount() {
    clearTimeout(this.fetchTimeout)
  }

  getCurrencyRate = () => {
    const { fetchRate } = this.props

    fetchRate()
    this.fetchTimeout = setTimeout(this.getCurrencyRate, REFRESH_TIMEOUT)
  }

  render() {
    return (
      <>
        <GlobalStyle />
        <AppContainer>
          <Box>
            <OriginWalletDisplay />
            <SwapWallets />
            <RateDisplay />
            <DestinationWalletDisplay />
            <ExchangeButton />
          </Box>
        </AppContainer>
      </>
    )
  }
}

export default connect(
  null,
  {
    fetchRate,
  },
)(App)
