import React from 'react'
import { mount } from 'enzyme'
import { WalletInput } from '../components/walletInput/WalletInput'

const DELAY_INPUT_TIMEOUT = 500

describe('<WalletInput/>', () => {
  it('should render without breaks and call onChange and onDelayChange event after timeout when input value change if input value is valid', done => {
    const inputValue = '0.02'
    const inputValueConverted = 2
    const onChange = jest.fn()
    const onDelayChange = jest.fn()
    const wrapper = mount(
      <WalletInput onChange={onChange} onDelayChange={onDelayChange} />,
    )

    wrapper.find('input').simulate('change', { target: { value: inputValue } })
    wrapper.setProps({ value: inputValueConverted })
    setTimeout(() => {
      expect(onDelayChange).toBeCalled()
      done()
    }, DELAY_INPUT_TIMEOUT)
  })
  it("should render without breaks and shouldn't change input if value is not valid", () => {
    const inputValue = 'example'
    const onChange = jest.fn()
    const wrapper = mount(<WalletInput onChange={onChange} />)

    wrapper.find('input').simulate('change', { target: { value: inputValue } })
    expect(onChange).not.toBeCalled()
  })
})
