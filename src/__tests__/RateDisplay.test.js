import React from 'react'
import { mount } from 'enzyme'
import { RateDisplay } from '../components/rateDisplay/RateDisplay'

describe('<RateDisplay/>', () => {
  it('should render without breaks and rendered text should match', () => {
    const text = '1 $ = 0.5 €'
    const wrapper = mount(
      <RateDisplay
        originWalletCurrencySymbol="$"
        currentRate={0.5}
        destinationWalletCurrencySymbol="€"
      />,
    )

    expect(wrapper.find('span').text()).toEqual(text)
  })
})
