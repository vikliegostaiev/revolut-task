import React from 'react'
import { mount } from 'enzyme'
import { WalletsBottomSelector } from '../components/walletsBottomSelector/WalletsBottomSelector'

const availableWallets = [
  {
    currency: 'USD',
    currencySymbol: '$',
    balance: 2000,
  },
  {
    currency: 'EUR',
    currencySymbol: '€',
    balance: 2000,
  },
  {
    currency: 'GBP',
    currencySymbol: '£',
    balance: 2000,
  },
]

describe('<WalletsBottomSelector/>', () => {
  it('should render without breaks and render all availableWallets as WalletRow', () => {
    const wrapper = mount(
      <WalletsBottomSelector availableWallets={availableWallets} />,
    )

    expect(wrapper.find('WalletRow').length).toEqual(3)
  })
  it('should render without breaks and call onWalletClick with appropriate wallet', () => {
    const onWalletClick = wallet => {
      expect(wallet).toEqual(availableWallets[1])
    }

    const onClose = jest.fn()

    const wrapper = mount(
      <WalletsBottomSelector
        availableWallets={availableWallets}
        onWalletClick={onWalletClick}
        onClose={onClose}
      />,
    )

    wrapper
      .find('WalletRow')
      .at(1)
      .simulate('click')
  })
})
