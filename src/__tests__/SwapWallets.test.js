import React from 'react'
import { mount } from 'enzyme'
import { SwapWallets } from '../components/swapWallets/SwapWallets'

describe('<SwapWallets/>', () => {
  it('should render without breaks and call swapWallets event on click', () => {
    const swapWallets = jest.fn()
    const wrapper = mount(<SwapWallets swapWallets={swapWallets} />)

    wrapper.simulate('click')
    expect(swapWallets).toBeCalled()
  })
})
