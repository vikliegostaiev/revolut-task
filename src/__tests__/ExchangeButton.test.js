import React from 'react'
import { mount } from 'enzyme'
import { ExchangeButton } from '../components/exchangeButton/ExchangeButton'

describe('<ExchangeButton/>', () => {
  it('should render without breaks and call makeTransfer event on click', () => {
    const makeTransfer = jest.fn()
    const wrapper = mount(
      <ExchangeButton disabled={false} makeTransfer={makeTransfer} />,
    )

    wrapper.simulate('click')
    expect(makeTransfer).toBeCalled()
  })
  it("should render without breaks and doesn't call makeTransfer event on click", () => {
    const makeTransfer = jest.fn()
    const wrapper = mount(<ExchangeButton makeTransfer={makeTransfer} />)

    wrapper.simulate('click')
    expect(makeTransfer).not.toBeCalled()
  })
})
