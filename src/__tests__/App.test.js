import React from 'react'
import { mount } from 'enzyme'
import { App } from '../containers/App'

jest.mock('../components/walletDisplay/OriginWalletDisplay', () => () => null)
jest.mock('../components/walletDisplay/DestinationWalletDisplay', () => () =>
  null,
)
jest.mock('../components/swapWallets/SwapWallets', () => () => null)
jest.mock('../components/rateDisplay/RateDisplay', () => () => null)
jest.mock('../components/exchangeButton/ExchangeButton', () => () => null)

jest.useFakeTimers()

describe('<App/>', () => {
  it('should render without breaks and call fetchRate onMount and after 10 seconds', () => {
    const REFRESH_TIMEOUT = 10000
    const FETCH_RATE_CALLED_TIMES = 2

    const fetchRate = jest.fn()
    mount(<App fetchRate={fetchRate} />)
    expect(fetchRate).toBeCalled()

    jest.advanceTimersByTime(REFRESH_TIMEOUT)
    expect(fetchRate).toBeCalled()
    expect(fetchRate).toHaveBeenCalledTimes(FETCH_RATE_CALLED_TIMES)
  })
})
