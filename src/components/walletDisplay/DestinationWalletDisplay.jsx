import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { WalletDisplay } from './WalletDisplay'
import { setDestinationWallet } from '../../store/action/wallet'
import {
  changeOriginInputValueBasedOnDestination,
  setDestinationInputFieldFocused,
  changeDestinationInputValue,
} from '../../store/action/inputs'

const ExtendedWalletDisplay = styled(WalletDisplay)`
  padding: 80px 20px 0 20px;
  background-color: #f3f4f6;
  height: 60%;
  box-sizing: border-box;
`

const DestinationWalletDisplay = ({ destinationWallet, ...rest }) => (
  <ExtendedWalletDisplay {...destinationWallet} {...rest} mathPrefix="+" />
)

export default connect(
  ({ selectedWallets }) => ({
    destinationWallet: selectedWallets.destinationWallet,
  }),
  {
    onWalletClick: setDestinationWallet,
    onInputChange: changeDestinationInputValue,
    onInputDelayChange: changeOriginInputValueBasedOnDestination,
    onInputFocus: setDestinationInputFieldFocused,
  },
)(DestinationWalletDisplay)
