import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { WalletDisplay } from './WalletDisplay'
import { setOriginWallet } from '../../store/action/wallet'
import {
  setOriginInputFieldFocused,
  changeOriginInputValue,
  changeDestinationInputValueBasedOnOrigin,
} from '../../store/action/inputs'

const ExtendedWalletDisplay = styled(WalletDisplay)`
  height: 40%;
  box-sizing: border-box;
  padding: 80px 20px;
`

const OriginWalletDisplay = ({ originWallet, ...rest }) => (
  <ExtendedWalletDisplay
    {...originWallet}
    {...rest}
    mathPrefix="-"
    isLowBalance={originWallet.amount > originWallet.balance}
  />
)

export default connect(
  ({ selectedWallets }) => ({
    originWallet: selectedWallets.originWallet,
  }),
  {
    onWalletClick: setOriginWallet,
    onInputChange: changeOriginInputValue,
    onInputDelayChange: changeDestinationInputValueBasedOnOrigin,
    onInputFocus: setOriginInputFieldFocused,
  },
)(OriginWalletDisplay)
