import React from 'react'
import styled from 'styled-components'
import { WalletBalance } from '../walletBalance/WalletBalance'
import { WalletDropdown } from '../walletDropdown/WalletDropdown'
import { WalletInput } from '../walletInput/WalletInput'

const InputRow = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
`

const WalletDisplay = ({
  currency,
  balance,
  currencySymbol,
  amount,
  transferAmount,
  mathPrefix,
  isLowBalance,
  onWalletClick,
  onInputChange,
  onInputDelayChange,
  onInputFocus,
  lastFocusedInputField,
  ...rest
}) => {
  return (
    <div {...rest}>
      <InputRow>
        <WalletDropdown
          selectedCurrency={currency}
          onWalletClick={onWalletClick}
        />
        <WalletInput
          value={amount}
          mathPrefix={mathPrefix}
          onChange={onInputChange}
          onDelayChange={onInputDelayChange}
          onInputFocus={onInputFocus}
        />
      </InputRow>
      {WalletBalance({ balance, currencySymbol, isLowBalance })}
    </div>
  )
}

export { WalletDisplay }
