import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { swapWallets } from '../../store/action/wallet'
import { ReactComponent as SwapIcon } from './swap_icon.svg'

const SwapContainer = styled.div`
  border: 1px solid #f3f4f6;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 40%;
  left: 20px;
  transform: translateY(-50%);
  background-color: #ffffff;
  height: 28px;
  width: 28px;
  cursor: pointer;
`

const ExtendedSwapIcon = styled(SwapIcon)`
  fill: #0074eb;
  width: 15px;
  height: 15px;
`

const SwapWallets = ({ swapWallets }) => (
  <SwapContainer onClick={swapWallets}>
    <ExtendedSwapIcon />
  </SwapContainer>
)

SwapWallets.propTypes = {
  swapWallets: PropTypes.func,
}

export { SwapWallets }

export default connect(
  null,
  {
    swapWallets,
  },
)(SwapWallets)
