import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { makeTransfer } from '../../store/action/transfer'

const StyledButton = styled.div`
    background-color: #eb008d;
    opacity: ${props => (props.disabled ? 0.4 : 1)}
    color: #FFFFFF;
    font-weight: 600;
    text-align: center;
    border-radius: 25px;
    padding-top: 10px;
    padding-bottom: 10px;
    width: 80%;
    position: absolute;
    bottom: 30px;
    left: 50%;
    transform: translateX(-50%);
    cursor: ${props => (props.disabled ? 'no-drop' : 'pointer')}
    box-shadow: 0px 6px 25px 0px rgba(235,0,141,0.35);
`

const ExchangeButton = ({ disabled, makeTransfer }) => (
  <StyledButton disabled={disabled} onClick={!disabled ? makeTransfer : null}>
    Exchange
  </StyledButton>
)

ExchangeButton.propTypes = {
  disabled: PropTypes.bool,
  makeTransfer: PropTypes.func,
}

ExchangeButton.defaultProps = {
  disabled: true,
}

export { ExchangeButton }
export default connect(
  ({ selectedWallets }) => ({
    disabled:
      !selectedWallets.originWallet.amount ||
      selectedWallets.originWallet.amount >
        selectedWallets.originWallet.balance,
  }),
  {
    makeTransfer,
  },
)(ExchangeButton)
