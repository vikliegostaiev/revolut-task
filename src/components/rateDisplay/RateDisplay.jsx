import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { ReactComponent as TrendUpIcon } from './trending_up.svg'

const RateDisplayContainer = styled.div`
  border: 1px solid #f3f4f6;
  border-radius: 25px;
  display: flex;
  align-items: center;
  padding: 5px 15px;
  position: absolute;
  top: 40%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  color: #0074eb;
  background-color: #ffffff;
`

const RateText = styled.span`
  line-height: 1;
`

const ExtendedTrendUpIcon = styled(TrendUpIcon)`
  fill: #0074eb;
  margin-right: 10px;
  vertical-align: middle;
  width: 15px;
  height: 15px;
`

const RateDisplay = ({
  originWalletCurrencySymbol,
  destinationWalletCurrencySymbol,
  currentRate,
}) => (
  <RateDisplayContainer>
    <ExtendedTrendUpIcon />
    {currentRate && (
      <RateText>
        1 {originWalletCurrencySymbol} = {currentRate}{' '}
        {destinationWalletCurrencySymbol}
      </RateText>
    )}
  </RateDisplayContainer>
)

RateDisplay.propTypes = {
  originWalletCurrencySymbol: PropTypes.string,
  destinationWalletCurrencySymbol: PropTypes.string,
  currentRate: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
}

export { RateDisplay }

export default connect(({ selectedCurrenciesRate, selectedWallets }) => ({
  currentRate: selectedCurrenciesRate.rate,
  originWalletCurrencySymbol: selectedWallets.originWallet.currencySymbol,
  destinationWalletCurrencySymbol:
    selectedWallets.destinationWallet.currencySymbol,
}))(RateDisplay)
