import React, { useState, useCallback } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import WalletsBottomSelector from '../walletsBottomSelector/WalletsBottomSelector'
import { ReactComponent as ExpandMoreIcon } from './expand_more.svg'

const DropdownField = styled.div`
  display: inline-block;
  cursor: pointer;
`

const StyledCurrency = styled.span`
  font-size: 24px;
  line-height: 1;
  margin-right: 5px;
`

const StyledExpandMoreIcon = styled(ExpandMoreIcon)`
  width: 14px;
  height: 14px;
`

const WalletDropdown = ({ selectedCurrency, onWalletClick }) => {
  const [walletsSelectorOpen, setWalletsSelectorOpen] = useState(false)

  const openWalletsSelector = useCallback(() => {
    setWalletsSelectorOpen(true)
  })

  const closeWalletsSelector = useCallback(() => {
    setWalletsSelectorOpen(false)
  })

  return (
    <>
      <DropdownField onClick={openWalletsSelector}>
        <StyledCurrency>{selectedCurrency}</StyledCurrency>
        <StyledExpandMoreIcon />
      </DropdownField>
      <WalletsBottomSelector
        selectedCurrency={selectedCurrency}
        isOpen={walletsSelectorOpen}
        onClose={closeWalletsSelector}
        onWalletClick={onWalletClick}
      />
    </>
  )
}

WalletDropdown.propTypes = {
  selectedCurrency: PropTypes.string,
  onWalletClick: PropTypes.func,
}

export { WalletDropdown }
