import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { convertAmountToTwoDigits } from '../../utils/moneyConverter/moneyConverter'
import { WalletBottomSelectorBackground } from './WalletBottomSelectorBackground'

const BACKGROUND_HIDE_TIMEOUT = 200

const WalletsBottomSelectorContainer = styled.div`
  position: absolute;
  z-index: 10;
  left: 0;
  bottom: 0;
  width: 100%;
  height: auto;
  background-color: #ffff;
  padding-left: 10px;
  box-sizing: border-box;
  transition: max-height
    ${props => (props.isOpen ? 400 : BACKGROUND_HIDE_TIMEOUT)}ms ease;
  overflow: hidden;
  max-height: ${props => (props.isOpen ? '300px' : '0')};
`

const CurrencyTitle = styled.div`
  font-size: 18px;
  font-weight: 600;
  margin-top: 10px;
  margin-bottom: 20px;
`

const WalletRow = styled.div`
  margin-bottom: 25px;
  color: ${props => (props.active ? '#0074eb' : '#000')};
  cursor: pointer;
  &:hover {
    opacity: 0.8;
  }
`

WalletRow.displayName = 'WalletRow';

const _closeTimeout = Symbol()

export class WalletsBottomSelector extends React.Component {
  static propTypes = {
    selectedCurrency: PropTypes.string,
    availableWallets: PropTypes.arrayOf(PropTypes.object),
    isOpen: PropTypes.bool,
    onClose: PropTypes.func,
    onWalletClick: PropTypes.func,
  }

  state = {
    enableBackground: false,
  }

  componentDidUpdate() {
    if (this.props.isOpen && !this.state.enableBackground) {
      if (this[_closeTimeout]) {
        clearTimeout(this[_closeTimeout])
      }
      this.setState({
        enableBackground: true,
      })
    }
  }

  disableBackground = () => {
    this.setState({
      enableBackground: false,
    })
  }

  onBackgroundClick = () => {
    this.props.onClose()
    this[_closeTimeout] = setTimeout(
      this.disableBackground,
      BACKGROUND_HIDE_TIMEOUT,
    )
  }

  onWalletClick = wallet => () => {
    this.props.onWalletClick(wallet)
    this.onBackgroundClick()
  }

  render() {
    const { selectedCurrency, availableWallets, isOpen } = this.props
    const { enableBackground } = this.state

    return (
      <>
        <WalletsBottomSelectorContainer isOpen={isOpen}>
          <CurrencyTitle>Choose currency:</CurrencyTitle>
          <div>
            {availableWallets.map(wallet => (
              <WalletRow
                key={wallet.currency}
                active={wallet.currency === selectedCurrency}
                onClick={this.onWalletClick(wallet)}
              >
                {wallet.currency} - {convertAmountToTwoDigits(wallet.balance)}
              </WalletRow>
            ))}
          </div>
        </WalletsBottomSelectorContainer>
        <WalletBottomSelectorBackground
          enable={enableBackground}
          onBackgroundClick={this.onBackgroundClick}
        />
      </>
    )
  }
}

export default connect(state => {
  const availableWallets = []

  Object.keys(state).forEach(key => {
    if (Object.prototype.hasOwnProperty.call(state[key], 'currency')) {
      availableWallets.push({
        ...state[key],
      })
    }
  })

  return {
    availableWallets,
  }
})(WalletsBottomSelector)
