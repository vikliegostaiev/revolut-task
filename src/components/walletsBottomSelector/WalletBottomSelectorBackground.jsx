import React from 'react'
import styled from 'styled-components'

const SelectorBackground = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: #000;
  opacity: 0.4;
  z-index: 5;
`

export const WalletBottomSelectorBackground = ({ enable, onBackgroundClick }) =>
  enable ? <SelectorBackground onClick={onBackgroundClick} /> : null
