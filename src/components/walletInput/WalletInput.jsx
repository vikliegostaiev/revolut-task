import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import isNil from 'lodash.isnil'
import {
  convertAmountToTwoDigits,
  convertToCoins,
} from '../../utils/moneyConverter/moneyConverter'

const StyledInputContainer = styled.div`
  width: 70%;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

const StyledInput = styled.input`
  font-size: 20px;
  width: 100%;
  text-align: right;
  background-color: inherit;
  outline: none;
  border: none;
`

const DELAY_INPUT_TIMEOUT = 500
const INPUT_REGEXP = /^\d*\.?\d{0,2}$/

export class WalletInput extends React.Component {
  static propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    mathPrefix: PropTypes.string,
    onChange: PropTypes.func,
    onDelayChange: PropTypes.func,
  }

  state = {
    currentValue: '',
  }

  componentDidUpdate() {
    const { value } = this.props
    const { currentValue } = this.state

    const simplifiedValue = this.getSimplifiedValue(currentValue)
    const coinsValue = simplifiedValue
      ? convertToCoins(simplifiedValue)
      : simplifiedValue

    if (coinsValue !== value && !isNil(value)) {
      this.setState({
        currentValue: value
          ? convertAmountToTwoDigits(value).toString()
          : value,
      })
    }
  }

  onDelayChange = value => () => {
    this.props.onDelayChange(value)
  }

  inputOnChange = ({ target: { value } }) => {
    const { onChange } = this.props

    const simplifiedValue = this.getSimplifiedValue(value)
    const coinsValue = simplifiedValue
      ? convertToCoins(simplifiedValue)
      : simplifiedValue

    if (this.validateInput(simplifiedValue)) {
      this.onDelayChange(coinsValue)
      onChange(coinsValue)
      this.setState({ currentValue: simplifiedValue })

      if (this.delayChangeTimeout) {
        clearTimeout(this.delayChangeTimeout)
      }

      this.delayChangeTimeout = setTimeout(
        this.onDelayChange(coinsValue),
        DELAY_INPUT_TIMEOUT,
      )
    }
  }

  render() {
    const { mathPrefix, onInputFocus } = this.props
    const { currentValue } = this.state

    return (
      <StyledInputContainer>
        <StyledInput
          type="text"
          placeholder="0"
          value={currentValue ? `${mathPrefix} ${currentValue}` : currentValue}
          onChange={this.inputOnChange}
          onFocus={onInputFocus}
        />
      </StyledInputContainer>
    )
  }

  getSimplifiedValue(value) {
    const { mathPrefix } = this.props

    return value && value.includes(mathPrefix) ? value.split(' ')[1] : value
  }

  validateInput(value) {
    return (
      INPUT_REGEXP.test(value) &&
      (value[0] !== '0' || value[1] === '.' || value.length === 1)
    )
  }
}
