import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { convertAmountToTwoDigits } from '../../utils/moneyConverter/moneyConverter'

const StyledText = styled.span`
  font-size: 11px;
  color: ${props => (props.isLowBalance ? '#eb008d' : '#8c969f')};
`

const WalletBalance = ({ balance, currencySymbol, isLowBalance }) => (
  <StyledText isLowBalance={isLowBalance}>
    Balance: {convertAmountToTwoDigits(balance)} {currencySymbol}
  </StyledText>
)

WalletBalance.propTypes = {
  balance: PropTypes.number,
  currencySymbol: PropTypes.string,
  isLowBalance: PropTypes.bool,
}

export { WalletBalance }
